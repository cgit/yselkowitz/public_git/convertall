Name:           convertall
Version:        0.8.0
Release:        1%{?dist}
Summary:        Unit converter

License:        GPLv2
URL:            https://convertall.bellz.org/
Source0:        https://github.com/doug-101/ConvertAll/releases/download/v%{version}/convertall-%{version}.tar.gz

BuildRequires:  desktop-file-utils
BuildRequires:  python3
BuildRequires:  python3-qt5
Requires:       hicolor-icon-theme
Requires:       python3
Requires:       python3-qt5

BuildArch:      noarch

%description
With ConvertAll, you can combine the units any way you want. If you want
to convert from inches per decade, that's fine. Or from meter-pounds. Or
from cubic nautical miles. The units don't have to make sense to anyone
else.

%prep
%autosetup -n ConvertAll

%build

%install
./install.py \
  -b %{buildroot} \
  -d %{_docdir}/%{name} \
  -i %{_datadir}/%{name}/icons \
  -p %{_prefix}
install -D -m0644 icons/%{name}_sm.png %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/%{name}-icon.png
install -D -m0644 icons/%{name}_med.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/%{name}-icon.png
install -D -m0644 icons/%{name}_lg.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}-icon.png
desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop || :
find %{buildroot}%{_docdir}/%{name} -delete

%find_lang %{name} --with-qt --without-mo

%files -f %{name}.lang
%license doc/LICENSE
%doc doc/README*.html
%{_bindir}/convertall
%{_datadir}/applications/convertall.desktop
%dir %{_datadir}/%{name}
%exclude %{_datadir}/%{name}/%{name}.pro
%exclude %{_datadir}/%{name}/%{name}.spec
%{_datadir}/%{name}/*.py
%{_datadir}/%{name}/__pycache__
%{_datadir}/%{name}/data/
%{_datadir}/%{name}/icons/
%dir %{_datadir}/%{name}/translations
%exclude %{_datadir}/%{name}/translations/*.ts
%exclude %{_datadir}/%{name}/translations/qt*
%{_datadir}/icons/hicolor/*/apps/%{name}-icon.*


%changelog
* Mon Aug  3 2020 Yaakov Selkowitz <yselkowi@redhat.com> - 0.8.0-1
- Initial release
